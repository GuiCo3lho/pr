///////////TRABALHO DE JAVASCRIPT///////////
let d = 0;
let c = 0;
t = document.getElementById("minha-linda-tabela").rows;
for (let i = 1; i < t.length - 1; i++) {
  c = parseFloat(t[i].cells[3].innerText);

  d += c;
}

t[8].cells[1].innerText = d.toFixed(2);

for (let i = 1; i < t.length - 1; i++) {
  if (parseFloat(t[i].cells[3].innerText) >= 0) {
    t[i].classList.add("table-success");
  } else {
    t[i].classList.add("table-danger");
  }
}

///////////TRABALHO DE JAVASCRIPT///////////

///////////DESAFIO DE JAVASCRIPT///////////
document.getElementById(
  "toggle-dark-mode"
).onclick = function toggleDarkLight() {
  var backgroundSelector = document.getElementById("meu-container"); //Seletor para cor de background
  var currentClass = backgroundSelector.className;
  var buttonSelector = document.querySelector(".btn"); //Seletor para cor do botao
  var currentButtonClass = buttonSelector.className;
  var SiteBarSelector = document.querySelector(".nav-item"); //Seletor para cor do Site Bar
  var currentSiteBarClass = SiteBarSelector.className;
  var tableAtributesSelector = document.querySelectorAll("th"); //Seletor para Atributos da Tabela

  for (let i = 0; i < 4; i++) {
    currentTableAtributesClass = tableAtributesSelector[i];
    currentClass4 = currentTableAtributesClass.className;
    if (currentClass4 == "bg-light text-dark") {
      currentTableAtributesClass.classList.remove("bg-light");
      currentTableAtributesClass.classList.remove("text-dark");
      currentTableAtributesClass.classList.add("bg-dark");
      currentTableAtributesClass.classList.add("text-light");
    } else {
      currentTableAtributesClass.classList.remove("bg-dark");
      currentTableAtributesClass.classList.remove("text-light");
      currentTableAtributesClass.classList.add("bg-light");
      currentTableAtributesClass.classList.add("text-dark");
    }
  }

  for (let i = 11; i < 13; i++) {
    // Iteração para alterar classes do
    currentTableAtributesClass = tableAtributesSelector[i];
    currentClass4 = currentTableAtributesClass.className;

    if (currentClass4 == "text-dark") {
      currentTableAtributesClass.classList.remove("text-dark");
      currentTableAtributesClass.classList.add("text-light");
    } else {
      currentTableAtributesClass.classList.remove("text-light");
      currentTableAtributesClass.classList.add("text-dark");
    }
  }

  if (currentSiteBarClass == "nav-item text-light") {
    SiteBarSelector.classList.remove("text-light");
    SiteBarSelector.classList.add("text-dark");
  } else {
    SiteBarSelector.classList.remove("text-dark");
    SiteBarSelector.classList.add("text-light");
  }

  if (currentButtonClass == "btn btn-dark") {
    buttonSelector.classList.remove("btn-dark");
    buttonSelector.classList.add("btn-light");
  } else {
    buttonSelector.classList.remove("btn-light");
    buttonSelector.classList.add("btn-dark");
  }

  if (currentClass == "") {
    backgroundSelector.className = "bg-dark";
  } else if (currentClass == "bg-dark") {
    backgroundSelector.className = "bg-light";
  } else {
    backgroundSelector.className = "bg-dark";
  }
};

///////////DESAFIO DE JAVASCRIPT///////////